public class Metodos {

    /**
     * Genera un numero aleatorio entre un minimo y un maximo
     * @param minimo
     * @param maximo
     * @return
     */
    public static int generaNumeroAleatorio(int minimo, int maximo){
        return (int) (Math.random() * maximo) + minimo;
    }

}
