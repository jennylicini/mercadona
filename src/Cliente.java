import java.util.ArrayList;

public class Cliente {

    int idCliente;
    ArrayList<Integer> productos;

    public Cliente(int idCliente) {
        this.idCliente = idCliente;
        this.productos = new ArrayList<>();
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public ArrayList<Integer> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<Integer> productos) {
        this.productos = productos;
    }

    public void rellenarCompra(){
        int tot_productos = Metodos.generaNumeroAleatorio(1, 20);
        for (int i = 0; i <tot_productos ; i++) {
            int tipo_producto = Metodos.generaNumeroAleatorio(1, 3);
            switch(tipo_producto) {
                case 1:
                    productos.add(1);
                    break;
                case 2:
                    productos.add(3);
                    break;
                case 3:
                    productos.add(6);
                    break;
            }

        }
    }

    @Override
    public String toString() {
        return "Clientes{" +
                "idCliente=" + idCliente +
                ", productos=" + productos +
                '}';
    }
}
