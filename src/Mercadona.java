import java.util.ArrayList;

public class Mercadona implements Runnable {

    private Cliente cliente;
    private long tiempoIncial;
    private ArrayList<Caja> listaCajas;


    public Mercadona(Cliente cliente, long tiempoIncial, ArrayList<Caja> listaCajas) {
        this.cliente = cliente; //paso el cliente
        this.tiempoIncial = tiempoIncial; //el tiempo incial
        this.listaCajas = listaCajas; //y las todos las cajas
    }


    @Override
    public void run() {
        //añado a la caja que es igual al numero del thread el cliente
        asignarClienteCaja(this.cliente, this.listaCajas);

        System.out.println("Mercadona ["+nombreCajera(Thread.currentThread().getName()) + "] " +
                "cliente que esta atendiendo [" + this.cliente.getIdCliente()+"], tiempo incial "+(System.currentTimeMillis() - this.tiempoIncial) / 1000 + "seg");

        for (int i = 0; i <this.cliente.getProductos().toArray().length ; i++) {
            tiempoDeEspera(cliente.getProductos().get(i));
            System.out.println("Mercadona [" +nombreCajera(Thread.currentThread().getName()) + "] Procesado el producto " + (i + 1) + " del cliente [" + this.cliente.getIdCliente()+
                    "] ->Tiempo: " + (System.currentTimeMillis() - this.tiempoIncial) / 1000 + "seg");
        }

        System.out.println("Mercadona [" +nombreCajera(Thread.currentThread().getName()) + "] HA TERMINADO CON EL CLIENTE ["
                + this.cliente.getIdCliente() + "] EN EL TIEMPO: "
                + (System.currentTimeMillis() - this.tiempoIncial) / 1000 + "seg");


    }

    private void asignarClienteCaja(Cliente cliente, ArrayList<Caja> lista_cajas) {
        lista_cajas.get(nombreCajera(Thread.currentThread().getName())-1).anadirClientes(cliente);
    }

    private int nombreCajera(String nombre){
        switch (nombre){
            case "pool-1-thread-1":
                return 1;
            case "pool-1-thread-2":
                return 2;
            case "pool-1-thread-3":
                return 3;
            case "pool-1-thread-4":
                return 4;
        }
        return 0;

    }

    private void tiempoDeEspera(int segundos) {
        try {
            Thread.sleep(segundos * 1000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
}