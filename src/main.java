import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class main {

    public static void main(String[] args) {

        ArrayList<Cliente> clientes = new ArrayList<>();
        ArrayList<Caja> cajas = new ArrayList<>();
        long tiempo_inicial;

        int tot_clientes = Metodos.generaNumeroAleatorio(8, 20); //GENERO UN NUMERO DE CLIENTES ENTRE 8 Y 20
        System.out.println("Tot clientes "+ tot_clientes);

        //genero los clientes
        for (int i = 1; i <=tot_clientes ; i++) {
            Cliente c = new Cliente(i);
            c.rellenarCompra();
            clientes.add(c);
            System.out.println(c); //imprimo los clientes y te hago ver que productos tiene cada uno
        }

        //genero las 4 cajas
        for (int i = 0; i <= 3; i++) {
            Caja caja = new Caja(i);
            cajas.add(caja);
        }

        tiempo_inicial = System.currentTimeMillis();

        ExecutorService e = Executors.newFixedThreadPool(4); // 4 threads
        for (Cliente cliente: clientes) {
            Runnable thread = new Mercadona(cliente, tiempo_inicial, cajas);
            e.execute(thread);
        }
        e.shutdown();	// Cierro el Executor
        //System.out.println("Number of waiting jobs: "+e.getQueue().size());



    }
}
