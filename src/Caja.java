import java.util.ArrayList;

public class Caja {

    private int idCaja;
    private ArrayList<Cliente> listasClientes = new ArrayList<>();

    public Caja(int idCaja, ArrayList<Cliente> listasClientes) {
        this.idCaja = idCaja;
        this.listasClientes = listasClientes;
    }

    public Caja(int idCaja) {
        this.idCaja = idCaja;
    }

    public int getIdCaja() {
        return idCaja;
    }

    public void setIdCaja(int idCaja) {
        this.idCaja = idCaja;
    }

    public ArrayList<Cliente> getListasClientes() {
        return listasClientes;
    }

    public void setListasClientes(ArrayList<Cliente> listasClientes) {
        this.listasClientes = listasClientes;
    }

    public void anadirClientes(Cliente cliente){
        listasClientes.add(cliente);
    }

    @Override
    public String toString() {
        return "Caja{" +
                "idCaja=" + idCaja +
                ", listasClientes=" + listasClientes +
                '}';
    }

}
